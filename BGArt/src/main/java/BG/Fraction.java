package BG;

/**
 * Hello world!
 *
 */
public final class Fraction
{
    final private int numerator;
    final private int denominator;

    final private static Fraction ZERO = new Fraction(0);
    final private static Fraction ONE = new Fraction(1);

    Fraction(int numerator) {

        this.numerator = numerator;
        this.denominator = 1;
    }

    public Fraction(){

        this.numerator=0;
        this.denominator=1;

    }

    public Fraction(int num, int den){

        this.numerator=num;
        this.denominator=den;

    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public Fraction getZero(){
        return ZERO;
    }
    public Fraction getOne(){
        return ONE;
    }



    public String ConvToString() {
        String S = "" + this.numerator + "/" + this.denominator;
        return S;
    }
    public double Calc(){

        double num = this.numerator;
        double den = this.denominator;
        double d = num/den;
        return d;
    }

    public boolean Comp(Fraction D) {
        if (this.Calc() < D.Calc())
            return true;
        else return false;
    }

    public boolean isEqual(Fraction F){

        return (this.Calc()==F.Calc());
    }

    public static void main(String[] args )
    {

    }
}
